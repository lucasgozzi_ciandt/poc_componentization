import React, { Component } from "react";
import "./App.css";
import ButtonDefaultExample from "./modules/test/index";
import GridLayout from "./modules/grid/index";
import PageCreator from "./modules/draggable_page";
import "../node_modules/react-grid-layout/css/styles.css";
import "../node_modules/react-resizable/css/styles.css";
import { v4 as uuidv4 } from "uuid";

import { BrowserRouter, Routes, Route } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ButtonDefaultExample />} />
          <Route path="/grid" element={<GridLayout />} />
          <Route path="/draggable" element={<PageCreator />} />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;
