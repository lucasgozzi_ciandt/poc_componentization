import React, { useState } from "react";
import GridLayout from "react-grid-layout";
import { v4 as uuidv4 } from "uuid";
import Card from "../card";
import Description from "../description";
import Hero from "../hero";
import Image from "../image";
import PropConfig from "../prop_config";
import Title from "../title";
import { Drawer, Fab, Button, Select, MenuItem } from "@mui/material";
import "./page_gen.css";

function PageCreator({ componentsProps }) {
  let layout = [];
  let cs = [];
  const [componentsLarge, setComponentsLarge] = useState(
    componentsProps ? componentsProps : []
  );
  const [componentsMedium, setComponentsMedium] = useState(
    componentsProps ? componentsProps : []
  );
  const [componentsSmall, setComponentsSmall] = useState(
    componentsProps ? componentsProps : []
  );
  const [componentSelect, setComponentSelect] = useState("image");
  const [configType, setConfigType] = useState("");
  const [view, setView] = useState("small");
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [isComponentDrawerOpen, setIsComponentDrawerOpen] = useState(false);

  if (view === "large" && componentsLarge) {
    for (const key in componentsLarge) {
      const element = componentsLarge[key];
      layout.push(element);
      cs.push(element.cp);
    }
  }
  if (view === "medium" && componentsMedium) {
    for (const key in componentsMedium) {
      const element = componentsMedium[key];
      layout.push(element);
      cs.push(element.cp);
    }
  }
  if (view === "small" && componentsSmall) {
    for (const key in componentsSmall) {
      const element = componentsSmall[key];
      layout.push(element);
      cs.push(element.cp);
    }
  }
  const onLayoutChange = (l) => {
    const layoutChanged = [];
    l.forEach((element) => {
      const componentsView =
        view === "large"
          ? componentsLarge
          : view === "medium"
          ? componentsMedium
          : componentsSmall;
      var foundIndex = componentsView.findIndex((x) => x.i === element.i);
      layoutChanged.push({
        ...componentsView[foundIndex],
        x: element.x,
        y: element.y,
        w: element.w,
        h: element.h,
      });
    });
    if (view === "large") {
      setComponentsLarge(layoutChanged);
    }
    if (view === "medium") {
      setComponentsMedium(layoutChanged);
    }
    if (view === "small") {
      setComponentsSmall(layoutChanged);
    }
  };

  const showProps = (compName) => {
    setConfigType(compName);
  };
  const getClassIcon = (viewRepresented) => {
    if (view === viewRepresented) {
      return "material-icons button-selected";
    }
    return "material-icons";
  };
  const getViewWidth = () => {
    switch (view) {
      case "large":
        return 1200;
      case "medium":
        return 768;
      case "small":
        return 480;
    }
  };
  return (
    <div className="custom_page">
      <Drawer anchor={"left"} open={isDrawerOpen}>
        <ul>
          <li>
            <Button>Image</Button>
          </li>
          <li>
            <Button>Hero</Button>
          </li>
          <li>
            <Button>Card</Button>
          </li>
          <li>
            <Button>Description</Button>
          </li>
          <li>
            <Button onClick={() => setIsDrawerOpen(false)}>Close</Button>
          </li>
        </ul>
      </Drawer>
      <div className="toolbar">
        <Button
          className={getClassIcon("small")}
          color={view === "small" ? "secondary" : "primary"}
          onClick={() => setView("small")}
        >
          <span className="material-icons">smartphone</span>
        </Button>
        <Button
          className={getClassIcon("medium")}
          color={view === "medium" ? "secondary" : "primary"}
          onClick={() => setView("medium")}
        >
          <span className="material-icons">tablet_mac</span>
        </Button>
        <Button
          className={getClassIcon("large")}
          color={view === "large" ? "secondary" : "primary"}
          onClick={() => setView("large")}
        >
          <span className="material-icons">desktop_windows</span>
        </Button>
      </div>
      <Drawer anchor={"left"} open={isDrawerOpen}>
        <ul>
          <li>
            <Button>Image</Button>
          </li>
          <li>
            <Button>Hero</Button>
          </li>
          <li>
            <Button>Card</Button>
          </li>
          <li>
            <Button>Description</Button>
          </li>
          <li>
            <Button onClick={() => setIsDrawerOpen(false)}>Close</Button>
          </li>
        </ul>
      </Drawer>
      <div className="toolbar">
        <div>
          <Select
            id="demo-simple-select"
            value={componentSelect}
            label="Add new component"
            onChange={(event) => {
              setComponentSelect(event.target.value);
            }}
          >
            <MenuItem value={"image"}>Image</MenuItem>
            <MenuItem value={"hero"}>Hero</MenuItem>
            <MenuItem value={"card"}>Card</MenuItem>
            <MenuItem value={"title"}>Title</MenuItem>
            <MenuItem value={"description"}>Description</MenuItem>
          </Select>
        </div>
        <div>
          <Button
            onClick={() => {
              let comp = <span>a</span>;
              let minW = 1;
              let minH = 1;
              switch (componentSelect) {
                case "image":
                  minW = 3;
                  minH = 2;
                  comp = (
                    <Image src="https://www.jnjmedicaldevices.com/sites/default/files/inline-images/Hero%20Image%20no%20border.JPG" />
                  );
                  break;
                case "hero":
                  minW = 6;
                  minH = 4;
                  comp = (
                    <Hero src="https://www.jnjmedicaldevices.com/sites/default/files/styles/crop_presets/public/advanced-full-bleed-hero/2021-10/Monarch%20Tower%20-%20User%20Touching%20Monarch%20Tower%27s%20Displays%20Touch%20Screen%20%281%29.jpg?h=daa5c3b6&itok=seqMNYwy" />
                  );
                  break;
                case "card":
                  minW = 6;
                  minH = 4;
                  comp = (
                    <Card src="https://www.jnjmedicaldevices.com/sites/default/files/styles/crop_presets/public/advanced-full-bleed-hero/2021-10/Monarch%20Tower%20-%20User%20Touching%20Monarch%20Tower%27s%20Displays%20Touch%20Screen%20%281%29.jpg?h=daa5c3b6&itok=seqMNYwy" />
                  );
                  break;
                case "title":
                  minW = 2;
                  minH = 2;
                  comp = <Title text={"hello all ! :) "} />;
                  break;
                case "description":
                  minW = 2;
                  minH = 2;
                  comp = (
                    <Description
                      text={
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris elit diam, molestie eu bibendum a, consectetur sit amet mi. Nunc aliquam est volutpat magna volutpat, non fermentum libero dapibus. Curabitur laoreet, dui non hendrerit cursus, leo ligula tempus magna, sit amet dictum erat est at neque. In hac habitasse platea dictumst. Aliquam nec finibus nulla, sit amet sollicitudin massa. Ut pulvinar aliquet nibh ut feugiat. Pellentesque at erat ligula. Pellentesque sed dictum leo"
                      }
                    />
                  );
                  break;
                default:
                  break;
              }
              const id = uuidv4();
              setComponentsLarge([
                ...componentsLarge,
                {
                  i: id,
                  x: 0,
                  y: 0,
                  w: 12,
                  h: minH,
                  minW: minW,
                  minH: minH,
                  cp: (
                    <div key={id} onClick={() => showProps(componentSelect)}>
                      {comp}
                    </div>
                  ),
                },
              ]);
              setComponentsMedium([
                ...componentsMedium,
                {
                  i: id,
                  x: 0,
                  y: 0,
                  w: 12,
                  h: minH,
                  minW: minW,
                  minH: minH,
                  cp: (
                    <div key={id} onClick={() => showProps(componentSelect)}>
                      {comp}
                    </div>
                  ),
                },
              ]);

              setComponentsSmall([
                ...componentsSmall,
                {
                  i: id,
                  x: 0,
                  y: 0,
                  w: 12,
                  h: minH,
                  minW: minW,
                  minH: minH,
                  cp: (
                    <div
                      key={id}
                      onClick={() => {
                        // setIsComponentDrawerOpen(true);
                        showProps(componentSelect);
                      }}
                    >
                      {comp}
                    </div>
                  ),
                },
              ]);
            }}
          >
            Add Component
          </Button>
        </div>
      </div>

      <Drawer anchor={"right"} open={isComponentDrawerOpen}>
        <PropConfig type={configType}></PropConfig>
        <Button onClick={() => setIsComponentDrawerOpen(false)}>Close</Button>
      </Drawer>

      <div className="canvas" style={{ width: getViewWidth() + "px" }}>
        <GridLayout
          className="layout"
          layout={layout}
          cols={12}
          rowHeight={30}
          width={getViewWidth()}
          onLayoutChange={onLayoutChange}
        >
          {cs}
        </GridLayout>
      </div>
      <div className="fabs">
        <Fab
          color="primary"
          aria-label="add"
          onClick={() => setIsDrawerOpen(true)}
        >
          <span className="material-icons">add</span>
        </Fab>
        <Fab
          color="primary"
          aria-label="configure"
          onClick={() => setIsComponentDrawerOpen(true)}
        >
          <span className="material-icons">settings_suggest</span>
        </Fab>
      </div>
    </div>
  );
}

export default PageCreator;
