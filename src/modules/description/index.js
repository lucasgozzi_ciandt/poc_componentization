import React from "react";


function Description({ text }) {
  return (
    <div className="description_component_container">
      <span>{text}</span>
    </div>
  );
}

export default Description;
