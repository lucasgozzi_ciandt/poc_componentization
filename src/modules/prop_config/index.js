import React from "react";
import "./style.css";
import { TextField } from "@mui/material";

function PropConfig({ type }) {
  let returnConfigs = <div />;
  switch (type) {
    case "title":
      returnConfigs = (
        <div>
          <TextField id="outlined-basic" label="Text" variant="outlined" />
        </div>
      );
      break;
    case "description":
      returnConfigs = (
        <div>
          <TextField id="outlined-basic" label="Text" variant="outlined" />
        </div>
      );
      break;
    case "image":
      returnConfigs = (
        <div>
          <TextField id="outlined-basic" label="Source" variant="outlined" />
        </div>
      );
      break;
    case "hero":
      returnConfigs = (
        <div key="dv_source">
          <TextField
            key="txt_source"
            id="outlined-basic"
            label="Source"
            variant="outlined"
          />
          <TextField
            key="txt_text"
            id="outlined-basic"
            label="Text"
            variant="outlined"
          />
        </div>
      );
    case "card":
      returnConfigs = (
        <div key="dv_source">
          <TextField
            key="txt_source"
            id="outlined-basic"
            label="Source"
            variant="outlined"
          />
          <TextField
            key="txt_text"
            id="outlined-basic"
            label="Text"
            variant="outlined"
          />
        </div>
      );
      break;
    default:
      returnConfigs = <div />;
  }
  return (
    <div className="prop_config_component_container">
      <h5>Variables:</h5>
      {returnConfigs}
      <h5>Paddings:</h5>
      <div>
        <TextField id="outlined-basic" label="Padding top" variant="outlined" />
        <TextField
          id="outlined-basic"
          label="Padding left"
          variant="outlined"
        />
      </div>
      <div>
        <TextField
          id="outlined-basic"
          label="Padding bottom"
          variant="outlined"
        />
        <TextField
          id="outlined-basic"
          label="Padding right"
          variant="outlined"
        />
      </div>
      <h5>Margins:</h5>
      <div>
        <TextField id="outlined-basic" label="Margin top" variant="outlined" />
        <TextField id="outlined-basic" label="Margin left" variant="outlined" />
      </div>
      <div>
        <TextField
          id="outlined-basic"
          label="Margin bottom"
          variant="outlined"
        />
        <TextField
          id="outlined-basic"
          label="Margin right"
          variant="outlined"
        />
      </div>
      ,
    </div>
  );
}

export default PropConfig;
