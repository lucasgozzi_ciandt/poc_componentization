import React from "react";
import "./style.css";
function PageConfig({ type }) {
  let returnConfigs = <div />;
  switch (type) {
    case "title":
      returnConfigs = (
        <div>
          Text: <input type="text" />
        </div>
      );
      break;
      case "description":
        returnConfigs = (
          <div>
            Text: <input type="text" />
          </div>
        );
        break;
    case "image":
      returnConfigs = (
        <div>
          Source: <input type="text" />
        </div>
      );
      break;
    case "hero":
      returnConfigs = [
        <div>
          Source: <input type="text" />
        </div>,
        <div>
          Title: <input type="text" />
        </div>,
        <div>
          Description: <input type="text" />
        </div>,
      ];
      break;
    default:
      returnConfigs = <div />;
  }
  return <div className="prop_config_component_container">{returnConfigs}</div>;
}

export default PropConfig;
