import React from "react";


function Title({ text }) {
  return (
    <div className="title_component_container">
      <h1>{text}</h1>
    </div>
  );
}

export default Title;
