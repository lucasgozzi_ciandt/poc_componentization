import React from "react";
import Description from "../description";
import Title from "../title";
import "./style.css";
function Card({ src }) {
  console.log(src);
  return (
    <div className="card_component_container" style={{ backgroundImage: `url(${src})` }}>
      <div className="card_side_title">
        <Title text={"HEllo World!!"} />
        <Description text={"This is just an POC. lorem ipsum dolor sit amet"}></Description>
      </div>
    </div>
  );
}

export default Card;
