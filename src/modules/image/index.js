import React from "react";
import './style.css';
function Image({ src }) {
  return (
    <div className="image_component_container">
      <img src={src} />
    </div>
  );
}

export default Image;
