import React from "react";
import Description from "../description";
import Image from "../image";
import Title from "../title";
import "./style.css";
function Hero({ src }) {
  return (
    <div className="hero_component_container">
      <div className="hero_side_title">
        <Title text={"HEllo World!!"} />
        <Description text={"This is just an POC"}></Description>
      </div>
      <div className="hero_img">
        <Image src={src} />
      </div>
    </div>
  );
}

export default Hero;
